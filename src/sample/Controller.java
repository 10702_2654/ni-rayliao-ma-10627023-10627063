package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public ComboBox first;
    public ComboBox end;
    public DatePicker date;
    public TextField identify;
    public Label showtime;
    public ComboBox time;
    public TextField phone;
    public Label gold;
    public Button yes;

    List<String> str = new ArrayList<>();
    List<String> strtime = new ArrayList<>();
    public void initialize(URL location , ResourceBundle resources){
        str.add("台灣");
        str.add("美國");
        str.add("英國");
        str.add("香港");
        str.add("德國");
        str.add("韓國");
        str.add("日本");
        str.add("埃及");
        str.add("泰國");
        str.add("北極");
        str.add("印度");

        strtime.add("00:00");
        strtime.add("01:00");
        strtime.add("02:00");
        strtime.add("03:00");
        strtime.add("04:00");
        strtime.add("05:00");
        strtime.add("06:00");
        strtime.add("07:00");
        strtime.add("08:00");
        strtime.add("09:00");
        strtime.add("10:00");
        strtime.add("11:00");
        strtime.add("12:00");
        strtime.add("13:00");
        strtime.add("14:00");
        strtime.add("15:00");
        strtime.add("16:00");
        strtime.add("17:00");
        strtime.add("18:00");
        strtime.add("19:00");
        strtime.add("20:00");
        strtime.add("21:00");
        strtime.add("22:00");
        strtime.add("23:00");

        first.setItems(FXCollections.observableArrayList(str));
        end.setItems(FXCollections.observableArrayList(str));
        time.setItems(FXCollections.observableArrayList(strtime));
    }

    public void choosestation(ActionEvent actionEvent) {

    }
}
